# -*- Mode:Makefile; indent-tabs-mode:t; tab-width:4 -*-

all:
	go install

install:
	install -d -m755  $(DESTDIR)/bin/
	install -m755 $(GOPATH)/bin/webedit $(DESTDIR)/bin/webedit

clean:
	go clean
