# Webedit

Web based editor of custom page (gitlab.com:veterinabb/web.git) using hugo generator
(https://gohugo.io/).


## Requirements

* libgtk-3 
* git for page modifications/updates


## Build

Requires go 1.5+ (uses vendor folder).

```sh
$ sudo apt-get install build-essential libgtk-3-dev
$ go get -u gitlab.com/veterinabb/webedit
```

## Run

* install git
* configure git (user.name/user.email)
* setup ssh access to web page repo and run: 

```sh
$ webedit [flags]
```

## Develop

* use govendor for dependency management
* use go-bindata for embedding binary data to final executable (run generate.sh)

```sh
$ go get -u github.com/kardianos/govendor
$ go get -u github.com/jteeuwen/go-bindata/...
$ cd $GOPATH/src/gitlab.com/veterinabb/webedit
$ ./generate.sh
$ go install
```
