#!/bin/sh
echo "Generating go sources for icon and translation files"
go-bindata -pkg main -o bindata.go setup/gui/icon.png translation/sk-sk.all.json

echo "Finished"
