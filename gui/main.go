package gui

import (
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/andlabs/ui"
	"github.com/nicksnyder/go-i18n/i18n"
	"github.com/skratchdot/open-golang/open"
	"gitlab.com/veterinabb/webedit/page"
	"gitlab.com/veterinabb/webedit/page/data"
	"gitlab.com/veterinabb/webedit/repo"
)

// GUI elements to be passed to functions.
type uiData struct {
	news       *ui.MultilineEntry
	services   *ui.MultilineEntry
	status     *ui.Label
	upload     *ui.Button
	sched      []*ui.Entry
	schedNames []*ui.Label
	schedFoot  *ui.MultilineEntry
	w          *ui.Window
}

// Config contains user defined config values used by various GUI routines.
type Config struct {
	SSHHost, SSHUser, SSHPass, SSHDir string
	SSHPort                           int
	RepoDir                           string
	RepoURL                           string
	RepoBranch                        string
}

const (
	hostPort  = "localhost:4269"
	trWorking = "working"
	trError   = "error"
	trReady   = "ready"
	lang      = "sk-SK"
)

//Start starts application GUI.
func Start(cfg *Config) error {
	T, _ := i18n.Tfunc(lang)
	return ui.Main(func() {
		// GUI controls
		d := &uiData{news: ui.NewMultilineNonWrappingEntry(),
			services:   ui.NewMultilineNonWrappingEntry(),
			status:     ui.NewLabel(T(trWorking)),
			upload:     ui.NewButton(T("upload")),
			sched:      make([]*ui.Entry, 14),
			schedNames: make([]*ui.Label, 7),
			schedFoot:  ui.NewMultilineNonWrappingEntry(),
			w:          ui.NewWindow("Web Editor", 640, 480, false)}

		// GUI setup
		tab := ui.NewTab()

		// News GUI
		tab.Append(T("news"), d.news)
		tab.SetMargined(tab.NumPages()-1, true)
		// Services GUI
		tab.Append(T("services"), d.services)
		tab.SetMargined(tab.NumPages()-1, true)
		// Schedule GUI
		sbox := ui.NewVerticalBox()
		di := 0
		for i := 0; i < 7; i++ {
			d.sched[di] = ui.NewEntry()
			d.sched[di+1] = ui.NewEntry()
			d.schedNames[i] = ui.NewLabel("")
			bx := ui.NewHorizontalBox()
			bx.Append(d.schedNames[i], true)
			bx.Append(d.sched[di], true)
			bx.Append(d.sched[di+1], true)
			sbox.Append(bx, true)
			di += 2
		}
		sbox.Append(d.schedFoot, true)
		tab.Append(T("schedule"), sbox)
		tab.SetMargined(tab.NumPages()-1, true)

		// buttons and status label
		preview := ui.NewButton(T("preview"))
		d.upload.Disable()
		bbox := ui.NewHorizontalBox()
		bbox.Append(d.status, true)
		bbox.Append(preview, false)
		bbox.Append(d.upload, false)

		// main window layout
		box := ui.NewVerticalBox()
		box.Append(tab, true)
		box.Append(bbox, false)

		d.w.SetChild(box)
		d.w.SetMargined(true)
		d.w.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})

		preview.OnClicked(func(*ui.Button) {
			d.status.SetText(T(trWorking))
			go previewPage(cfg.RepoDir, d)
		})

		d.upload.OnClicked(func(*ui.Button) {
			d.status.SetText(T(trWorking))
			d.upload.Disable()
			go uploadPage(cfg, d)
		})

		d.w.Disable()
		d.w.Show()
		go loadData(cfg.RepoDir, cfg.RepoURL, cfg.RepoBranch, d)
	})
}

func loadData(repoDir, repoURL, repoBranch string, d *uiData) {
	T, _ := i18n.Tfunc(lang)
	err := repo.Update(repoDir, repoURL, repoBranch)
	ui.QueueMain(func() {
		if err != nil {
			showError(err, d.status, d.w)
		} else {
			// load data
			news, err := data.LoadNews(repoDir)
			if err != nil {
				showError(err, d.status, d.w)
				return
			}
			d.news.SetText(news.String())

			services, err := data.LoadServices(repoDir)
			if err != nil {
				showError(err, d.status, d.w)
				return
			}
			d.services.SetText(services.String())

			schedule, err := data.LoadScheduleData(repoDir)
			if err != nil {
				showError(err, d.status, d.w)
				return
			}
			d.schedFoot.SetText(schedule.FormatFootnotes())
			i := 0
			for idx := range schedule.Days {
				d.sched[i].SetText(schedule.Days[idx].AM)
				d.sched[i+1].SetText(schedule.Days[idx].PM)
				d.schedNames[idx].SetText(schedule.Days[idx].Day)
				i += 2
			}

			if page.IsGenerated(repoDir) {
				d.upload.Enable()
			}

			d.status.SetText(T(trReady))
			d.w.Enable()
		}
	})
}

func previewPage(repoDir string, d *uiData) {
	T, _ := i18n.Tfunc(lang)
	ui.QueueMain(func() {
		// store news

		if err := data.SaveNews(data.ParseItemData(d.news.Text()),
			repoDir); err != nil {
			showError(err, d.status, d.w)
			return
		}

		// store services
		if err := data.SaveServices(data.ParseItemData(d.services.Text()),
			repoDir); err != nil {
			showError(err, d.status, d.w)
			return
		}

		// update copyrightYears
		if err := data.UpdateCopyrightYears(repoDir); err != nil {
			showError(err, d.status, d.w)
			return
		}

		// update schedule
		sch := make([]string, 14)
		for i, s := range d.sched {
			sch[i] = s.Text()
		}
		if err := data.ParseScheduleData(sch, d.schedFoot.Text()).
			Save(repoDir); err != nil {
			showError(err, d.status, d.w)
			return
		}

		// regenerate page
		if err := page.Generate(repoDir); err != nil {
			showError(err, d.status, d.w)
			return
		}

		if page.IsGenerated(repoDir) {
			d.upload.Enable()
		}

		d.status.SetText(T(trReady))
		go servePage(repoDir)
	})
}

func serverRunning() bool {
	listener, err := net.Listen("tcp", hostPort)
	if err != nil {
		if strings.Index(err.Error(), "in use") != -1 {
			return true
		}
	}
	listener.Close()
	return false
}

func servePage(repoDir string) {
	go func() {
		time.Sleep(500 * time.Millisecond)
		open.Start("http://" + hostPort)
	}()

	if !serverRunning() {
		http.Handle("/",
			http.StripPrefix("/", http.FileServer(
				http.Dir(page.GeneratedDir(repoDir)))))

		log.Printf("Listening on %s", hostPort)
		http.ListenAndServe(hostPort, nil)
	}
}

func uploadPage(cfg *Config, d *uiData) {
	T, _ := i18n.Tfunc(lang)
	ui.QueueMain(func() {
		// git commit & push to server
		if err := repo.Push(cfg.RepoDir, cfg.RepoBranch); err != nil {
			showError(err, d.status, d.w)
			return
		}

		// sftp upload of generated page
		if err := page.Upload(cfg.SSHHost, cfg.SSHUser, cfg.SSHPass,
			cfg.SSHDir, cfg.SSHPort, cfg.RepoDir); err != nil {
			showError(err, d.status, d.w)
			return
		}

		d.upload.Enable()
		d.status.SetText(T(trReady))
	})
}

func showError(err error, status *ui.Label, w *ui.Window) {
	T, _ := i18n.Tfunc(lang)
	status.SetText(T(trError))
	ui.MsgBoxError(w, T(trError), err.Error())
}
