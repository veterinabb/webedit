package main

import (
	"flag"
	"log"
	"os"
	"os/user"
	"path"

	"github.com/nicksnyder/go-i18n/i18n"

	"gitlab.com/veterinabb/webedit/gui"
)

var (
	cfg = gui.Config{}
)

func init() {
	flag.StringVar(&cfg.SSHHost, "sshhost", "localhost",
		"remote server SSH hostname")
	flag.IntVar(&cfg.SSHPort, "sshport", 22,
		"remote server SSH port")
	flag.StringVar(&cfg.SSHUser, "sshuser", os.Getenv("USER"),
		"remote server SSH username")
	flag.StringVar(&cfg.SSHPass, "sshpass", os.Getenv("SOCKSIE_SSH_PASSWORD"),
		"remote server SSH password")
	flag.StringVar(&cfg.SSHDir, "sshdir", "",
		"remote server SSH directory")
	u, _ := user.Current()
	flag.StringVar(&cfg.RepoDir, "repodir", path.Join(u.HomeDir, "web"),
		"local directory containing edited web page")
	flag.StringVar(&cfg.RepoURL, "repourl", "git@gitlab.com:veterinabb/web.git",
		"edited web page remote repository")
	flag.StringVar(&cfg.RepoBranch, "repobranch", "master",
		"use different repository branch - for testing purposes")

	i18n.ParseTranslationFileBytes("sk-sk.all.json",
		MustAsset("translation/sk-sk.all.json"))

}

func dumpConfig() {
	log.Println("Starting with configuration:")
	flag.VisitAll(func(f *flag.Flag) {
		if f.Name != "sshpass" {
			log.Printf("\t%s: %v\n", f.Name, f.Value)
		}
	})
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()
	dumpConfig()

	if err := gui.Start(&cfg); err != nil {
		log.Println("Error starting GUI: ", err)
	}
}
