package data

import (
	"path"
	"strconv"
	"time"
)

type hugoCfgParams struct {
	Description    string `json:"description"`
	Author         string `json:"author"`
	CopyrightYears string `json:"copyrightYears"`
}

type hugoCfg struct {
	BaseURL        string        `json:"baseurl"`
	LanguageCode   string        `json:"languageCode"`
	Title          string        `json:"title"`
	DisableRSS     string        `json:"disableRSS"`
	DisableSitemap string        `json:"disableSitemap"`
	MetadataFormat string        `json:"metaDataFormat"`
	Params         hugoCfgParams `json:"params"`
}

func loadHugoCfg(filePath string) (*hugoCfg, error) {
	var c hugoCfg
	err := jsonLoad(&c, filePath)
	return &c, err
}

func (c *hugoCfg) save(filePath string) error {
	return jsonSave(c, filePath)
}

func copyrightYears(now time.Time) (copyright string) {
	const shortForm = "2006-Jan-02"
	tFrom, _ := time.Parse(shortForm, "2016-Jan-01")

	yFrom := tFrom.Year()
	yNow := now.Year()

	copyright = strconv.Itoa(yNow)
	if yFrom < yNow {
		copyright = strconv.Itoa(yFrom) + " - " + strconv.Itoa(yNow)
	}

	return
}

// UpdateCopyrightYears does what it says
func UpdateCopyrightYears(repoDir string) error {
	p := path.Join(repoDir, "config.json")
	c, err := loadHugoCfg(p)
	if err != nil {
		return err
	}

	c.Params.CopyrightYears = copyrightYears(time.Now())
	c.save(p)

	return nil
}
