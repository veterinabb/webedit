package data

import (
	"testing"
	"time"
)

func TestCopyrightYears(t *testing.T) {
	const tf = "2006-Jan-02"
	cases := []struct {
		name     string
		input    string
		expected string
	}{
		{"past", "2015-Dec-31", "2015"},
		{"current_1", "2016-Jan-01", "2016"},
		{"current_2", "2016-Dec-31", "2016"},
		{"future_1", "2017-Jan-01", "2016 - 2017"},
		{"future_2", "2117-Jan-01", "2016 - 2117"},
	}

	for _, c := range cases {
		now, err := time.Parse(tf, c.input)
		if err != nil {
			t.Fatalf("Error parsing test data %s: %s", c.input, err.Error())
		}

		actual := copyrightYears(now)
		if actual != c.expected {
			t.Errorf("Test '%s' expects '%s' but was '%s'",
				c.name, c.expected, actual)
		}
	}
}
