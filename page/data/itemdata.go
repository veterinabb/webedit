package data

import (
	"path"
	"strings"
)

const (
	dataSubDir = "data"
)

// ItemData json data
type ItemData struct {
	Items []string `json:"items"`
}

// String returns Items formatted to a single string
func (i *ItemData) String() string {
	return strings.Join(i.Items, "\n")
}

// Save ItemData to a specified file path
func (i *ItemData) Save(filePath string) error {
	return jsonSave(i, filePath)
}

// ParseItemData creates ItemData instance from formatted string
func ParseItemData(s string) *ItemData {
	return &ItemData{Items: sliceOf(s)}
}

// loadItemData loads ItemData from specified file path
func loadItemData(filePath string) (*ItemData, error) {
	var i ItemData
	err := jsonLoad(&i, filePath)
	return &i, err
}

func newsFilePath(repoDir string) string {
	return path.Join(repoDir, dataSubDir, "news.json")
}

// LoadNews loads ItemData from news.json file.
func LoadNews(repoDir string) (*ItemData, error) {
	return loadItemData(newsFilePath(repoDir))
}

// SaveNews stores data from i to news.json
func SaveNews(i *ItemData, repoDir string) error {
	return i.Save(newsFilePath(repoDir))
}

func servicesFilePath(repoDir string) string {
	return path.Join(repoDir, dataSubDir, "services.json")
}

// LoadServices loads ItemData from services.json file.
func LoadServices(repoDir string) (*ItemData, error) {
	return loadItemData(servicesFilePath(repoDir))
}

// SaveServices stores data from i to services.json
func SaveServices(i *ItemData, repoDir string) error {
	return i.Save(servicesFilePath(repoDir))
}

// parses string to a string slice (delimiter is \n, empty lines are ignored)
func sliceOf(s string) []string {
	splitted := strings.Split(s, "\n")
	trimmed := splitted[:0]
	for _, n := range splitted {
		t := strings.TrimSpace(n)
		if len(t) > 0 {
			trimmed = append(trimmed, t)
		}
	}

	return trimmed
}
