package data

import (
	"reflect"
	"testing"
)

func TestItemDataString(t *testing.T) {
	cases := []struct {
		name     string
		input    *ItemData
		expected string
	}{
		{"empty", &ItemData{Items: []string{}}, ""},
		{"one", &ItemData{Items: []string{"test"}}, "test"},
		{"multi", &ItemData{Items: []string{"test1", "test2"}}, "test1\ntest2"},
	}

	for _, c := range cases {
		actual := c.input.String()
		if actual != c.expected {
			t.Errorf("Test '%s' expects '%s' but was '%s'",
				c.name, c.expected, actual)
		}
	}
}

func TestParseItemData(t *testing.T) {
	cases := []struct {
		name     string
		input    string
		expected *ItemData
	}{
		{"empty", "", &ItemData{Items: []string{}}},
		{"oneline", "test", &ItemData{Items: []string{"test"}}},
		{"multi_line", "test1\ntest2",
			&ItemData{Items: []string{"test1", "test2"}}},
		{"multi_line_ugly", "\n\n     test1      \n\n\n\n\n\t\t\t test2  \n\n\n\n",
			&ItemData{Items: []string{"test1", "test2"}}},
	}

	for _, c := range cases {
		actual := ParseItemData(c.input)
		if !reflect.DeepEqual(actual, c.expected) {
			t.Errorf("Test '%s' expects '%s' but was '%s'",
				c.name, c.expected, actual)
		}
	}
}
