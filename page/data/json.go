package data

import (
	"encoding/json"
	"os"
)

func jsonSave(data interface{}, filePath string) error {
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	b, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return err
	}
	file.Write(b)

	return nil
}

func jsonLoad(data interface{}, filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(data)
	if err != nil {
		return err
	}

	return nil
}
