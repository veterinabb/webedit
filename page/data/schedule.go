package data

import (
	"path"
	"strings"
)

// Day is a day on schedule json struct
type Day struct {
	Day string `json:"day"`
	AM  string `json:"am"`
	PM  string `json:"pm"`
}

// ScheduleData is base schedule json struct
type ScheduleData struct {
	Days      []Day    `json:"days"`
	Footnotes []string `json:"footnotes"`
}

func scheduleFilePath(repoDir string) string {
	return path.Join(repoDir, dataSubDir, "schedule.json")
}

// LoadScheduleData loads ScheduleData from spefied directory
func LoadScheduleData(repoDir string) (*ScheduleData, error) {
	var s ScheduleData
	err := jsonLoad(&s, scheduleFilePath(repoDir))
	return &s, err
}

// Save ScheduleData to a specified directory
func (s *ScheduleData) Save(repoDir string) error {
	return jsonSave(s, scheduleFilePath(repoDir))
}

// ParseScheduleData parses array of string with am/pm values and text-formatted
// footnotes to ScheduleData struct
func ParseScheduleData(d []string, f string) *ScheduleData {
	sch := &ScheduleData{
		Days: []Day{
			Day{"Pondelok", "", ""},
			Day{"Utorok", "", ""},
			Day{"Streda", "", ""},
			Day{"Štvrtok", "", ""},
			Day{"Piatok", "", ""},
			Day{"Sobota", "", ""},
			Day{"Nedeľa", "", ""},
		},
		Footnotes: sliceOf(f)}

	i := 0
	for idx := range sch.Days {
		sch.Days[idx].AM = d[i]
		sch.Days[idx].PM = d[i+1]
		i += 2
	}

	return sch
}

// FormatFootnotes returns Footnotes slice formatted to a single string
func (s *ScheduleData) FormatFootnotes() string {
	return strings.Join(s.Footnotes, "\n")
}
