package data

import (
	"reflect"
	"testing"
)

func TestScheduleDataFormatFootnotes(t *testing.T) {
	cases := []struct {
		name     string
		input    *ScheduleData
		expected string
	}{
		{"empty", &ScheduleData{Footnotes: []string{}}, ""},
		{"one", &ScheduleData{Footnotes: []string{"test"}}, "test"},
		{"multi", &ScheduleData{Footnotes: []string{"test1", "test2"}},
			"test1\ntest2"},
	}

	for _, c := range cases {
		actual := c.input.FormatFootnotes()
		if actual != c.expected {
			t.Errorf("Test '%s' expects '%s' but was '%s'",
				c.name, c.expected, actual)
		}
	}
}

func TestParseScheduleData(t *testing.T) {
	expected := &ScheduleData{
		Days: []Day{
			Day{"Pondelok", "a", "b"},
			Day{"Utorok", "c", "d"},
			Day{"Streda", "e", "f"},
			Day{"Štvrtok", "g", "h"},
			Day{"Piatok", "i", "j"},
			Day{"Sobota", "k", "l"},
			Day{"Nedeľa", "m", "n"},
		},
		Footnotes: []string{"x"}}

	actual := ParseScheduleData([]string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"}, "x")
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("TestParseScheduleData expects '%s' but was '%s'",
			expected, actual)
	}

}
