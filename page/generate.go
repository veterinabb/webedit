package page

import (
	"os"
	"path"

	"github.com/spf13/hugo/commands"
)

// Generate generates page in generatedDir using HUGO generator.
// Prior to generation, it deletes old directory
func Generate(repoDir string) error {
	// TODO: remove this, when content editor implemented
	if err := os.MkdirAll(path.Join(repoDir, "content"), 0775); err != nil {
		return err
	}

	if err := os.RemoveAll(GeneratedDir(repoDir)); err != nil {
		return err
	}

	commands.HugoCmd.SetArgs([]string{"--source", repoDir})
	if err := commands.HugoCmd.Execute(); err != nil {
		return err
	}

	return nil
}

// IsGenerated returns true, when page is generated.
func IsGenerated(repoDir string) (generated bool) {
	generated = true
	if _, err := os.Stat(GeneratedDir(repoDir)); os.IsNotExist(err) {
		generated = false
	}

	return
}

// GeneratedDir returns name of directory containing generated page.
func GeneratedDir(repoDir string) string {
	return path.Join(repoDir, "public")
}
