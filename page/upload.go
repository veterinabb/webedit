package page

import (
	"fmt"
	"io"
	"os"
	"path"
	"strings"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

const (
	indexHTML = "index.html"
)

// Upload uploads generated page to remote server
func Upload(sshHost, sshUser, sshPass, sshDir string, sshPort int, repoDir string) error {
	cfg := &ssh.ClientConfig{
		User: sshUser,
		Auth: []ssh.AuthMethod{
			ssh.Password(sshPass),
		},
	}

	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", sshHost, sshPort), cfg)
	if err != nil {
		return err
	}
	defer client.Close()

	sftp, err := sftp.NewClient(client)
	if err != nil {
		return err
	}
	defer sftp.Close()

	// don't copy everything, only index.html
	targetFilePath := strings.TrimSpace(sshDir)
	if len(targetFilePath) != 0 {
		targetFilePath += "/"
	}
	targetFilePath += indexHTML

	target, err := sftp.Create(targetFilePath)
	if err != nil {
		return err
	}
	defer target.Close()

	src, err := os.Open(path.Join(GeneratedDir(repoDir), indexHTML))
	if err != nil {
		return err
	}
	defer src.Close()

	if _, err := io.Copy(target, src); err != nil {
		return err
	}

	return nil
}
