package repo

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"

	"github.com/nicksnyder/go-i18n/i18n"
)

var (
	vcs = "git"
)

const (
	lang = "sk-SK"
)

// Update fetches latest changes from repoURL.
// If repo doesn't exist in repoDir, function clones it from repoURL using
// repoBranch.
func Update(repoDir, repoURL, repoBranch string) error {
	T, _ := i18n.Tfunc(lang)
	exists := true
	if _, err := os.Stat(repoDir); os.IsNotExist(err) {
		exists = false
	}

	if !exists {
		log.Println("Page not existing yet, cloning from ",
			repoURL, " to ", repoDir)
		if _, err := runVcs(path.Dir(repoDir),
			[]string{"clone", repoURL, repoDir}); err != nil {
			return fmt.Errorf(T("err_clone_repo", map[string]interface{}{
				"Repo": repoURL,
				"Dir":  repoDir,
				"Err":  err.Error(),
			}))
		}
	}

	if _, err := runVcs(repoDir, []string{"checkout", repoBranch}); err != nil {
		return fmt.Errorf(T("err_checkout_branch", map[string]interface{}{
			"Branch": repoBranch,
			"Dir":    repoDir,
			"Err":    err.Error(),
		}))

	}

	if exists {
		if _, err := runVcs(repoDir,
			[]string{"pull", "--ff-only"}); err != nil {
			return fmt.Errorf(T("err_pull_repo", map[string]interface{}{
				"Dir": repoDir,
				"Err": err.Error(),
			}))

		}
	}

	return nil
}

// Push commits and pushes changes to repoURL.
func Push(repoDir, repoBranch string) error {
	T, _ := i18n.Tfunc(lang)
	if out, err := runVcs(repoDir, []string{"diff"}); err != nil {
		return fmt.Errorf(T("err_on_diff", map[string]interface{}{
			"Err": err.Error(),
		}))

	} else if len(out) != 0 {
		if _, err := runVcs(repoDir,
			[]string{"commit", "-a", "-m",
				"webedit " + time.Now().Format(time.RFC3339)}); err != nil {
			return fmt.Errorf(T("err_on_commit", map[string]interface{}{
				"Dir": repoDir,
				"Err": err.Error(),
			}))
		}
		if _, err := runVcs(repoDir,
			[]string{"push", "origin", repoBranch}); err != nil {
			return fmt.Errorf(T("err_on_push", map[string]interface{}{
				"Branch": repoBranch,
				"Dir":    repoDir,
				"Err":    err.Error(),
			}))
		}
	}
	log.Println("No changes detected, skipping commit & push")
	return nil
}

// Stolen from golang.org/x/tools/go/vcs
func envForDir(dir string) []string {
	env := os.Environ()
	return mergeEnvLists([]string{"PWD=" + dir}, env)
}

func mergeEnvLists(in, out []string) []string {
NextVar:
	for _, inkv := range in {
		k := strings.SplitAfterN(inkv, "=", 2)[0]
		for i, outkv := range out {
			if strings.HasPrefix(outkv, k) {
				out[i] = inkv
				continue NextVar
			}
		}
		out = append(out, inkv)
	}
	return out
}

func runVcs(dir string, args []string) ([]byte, error) {
	_, err := exec.LookPath(vcs)
	if err != nil {
		return nil, err
	}

	cmd := exec.Command(vcs, args...)
	cmd.Dir = dir
	cmd.Env = envForDir(cmd.Dir)
	var buf bytes.Buffer
	cmd.Stdout = &buf
	cmd.Stderr = &buf
	err = cmd.Run()
	out := buf.Bytes()
	if err != nil {
		return nil, err
	}
	return out, nil
}
